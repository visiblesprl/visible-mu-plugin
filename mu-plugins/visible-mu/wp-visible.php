<?php

/** Version 1.1 
 Toggle menu added 
 Style corrected
 **/

//blocking direct access to your plugin PHP files
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function goodbye_dolly() {
    if (file_exists(WP_PLUGIN_DIR.'/hello.php')) {
        require_once(ABSPATH.'wp-admin/includes/plugin.php');
        require_once(ABSPATH.'wp-admin/includes/file.php');
        delete_plugins(array('hello.php'));
    }
}
add_action('admin_init','goodbye_dolly');

function goodbye_askimet() {
    if (file_exists(WP_PLUGIN_DIR.'askimet/class.akismet.php')) {
        require_once(ABSPATH.'wp-admin/includes/plugin.php');
        require_once(ABSPATH.'wp-admin/includes/file.php');
        delete_plugins(array('/askimet/'));
    }
}
add_action('admin_init','goodbye_askimet');

function rebranding_wordpress_logo(){
    global $wp_admin_bar;
    //enelver les menus wordpress par défaut
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('wporg');


    //ajouter le logo visible
    $wp_admin_bar->add_menu( array(
        'id'    => 'wp-logo',
        'alt'   => 'visible_logo',
        'title' => '<img src="https://www.visible.be/assets2020/logo.svg" alt="visible logo" />',
        'href'  => __('https://visible.be'),
        'height'=> '30',
        'width' => '52',
        'meta'  => array(
            'title' => __('By Visible'),
        ),
    ) );
    //ajouter le sous-menu contacter-nous
    $wp_admin_bar->add_menu( array(
            'parent' => 'wp-logo',
            'id'     => 'sub-menu-id-1',
            'title'  => __('Contacter visible'),
            'href'  => __('https://www.visible.be/contact'),
    ) );
}
add_action('wp_before_admin_bar_render', 'rebranding_wordpress_logo' );

//changer style admin back
add_action('admin_head', 'my_custom_logo');
function my_custom_logo() {
    echo '
        <style type="text/css">
        #wpadminbar #wp-admin-bar-wp-logo>.ab-item {
            padding: 0 7px;
            height: 32px !important;
        }
        li#wp-admin-bar-wp-logo {
            height: 32px;
        }
        #wp-admin-bar-wp-logo img {
            /*max-width: 42px;*/
            padding:5px;
            width: 42px;
            height: auto;
        }
        #wpadminbar
        {
            background-color: #1D1E1B;
        }
        </style>
        ';
}

//changer style admin front
add_action('wp_head', 'my_custom_logoactive');
function my_custom_logoactive() {
    echo '
        <style type="text/css">
        #wpadminbar #wp-admin-bar-wp-logo>.ab-item {
            padding: 0 7px;
            height: 32px !important;
        }
        li#wp-admin-bar-wp-logo {
            height: 32px;
        }
        #wp-admin-bar-wp-logo img {
            max-width: 42px;
            padding:5px;
        }
        #wpadminbar
        {
            background-color: #1D1E1B;
        }
        </style>
        ';
}

//changer message footer admin
function remove_footer_admin () {
    echo "Designed by Visible";
}
add_filter('admin_footer_text', 'remove_footer_admin');

//auto-cocher se souvenir de moi
function sf_check_rememberme(){
    global $rememberme;
    $rememberme = 1;
}
add_action("login_form", "sf_check_rememberme");

//Gestion des plugins auto installés
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'autoplug_register_required_plugins' );
function autoplug_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        array(
            'name'        => 'YOAST SEO',
            'slug'        => 'wordpress-seo',
            'required'  => true,
        ),
        array(
            'name'        => 'Disable Comments',
            'slug'        => 'disable-comments',
            'required'  => false,
        ),
        array(
            'name'        => 'Réduction d\'image',
            'slug'        => 'imsanity',
            'required'  => false,
        ),
        array(
            'name'        => 'Firewall Wordfence',
            'slug'        => 'wordfence',
            'required'  => true,
        ),
        array(
            'name'        => 'Updraft Backup',
            'slug'        => 'updraftplus',
            'required'  => false,
        ),
        array(
            'name'        => 'Elementor',
            'slug'        => 'elementor',
            'required'  => false,
        ),
        array(
            'name'        => 'SVG Support',
            'slug'        => 'svg-support',
            'required'  => false,
        ),
        array(
            'name'        => 'Webp',
            'slug'        => 'webp-express',
            'required'  => false,
        ),
        array(
            'name'        => 'Add to Any',
            'slug'        => 'add-to-any',
            'required'  => false,
        ),
        array(
            'name'        => 'ACF',
            'slug'        => 'acf-extended',
            'required'  => false,
            'has_notices' => false,
        ),
        array(
            'name'        => 'Complianz',
            'slug'        => 'complianz-gdpr',
            'required'  => false,
        ),
        array(
            'name'        => 'Google Site Kit',
            'slug'        => 'google-site-kit',
            'required'  => false,
        ),
       /* array(
            'name'        => 'Tag manager',
            'slug'        => 'duracelltomi-google-tag-manager',
            'required'      => false,
        ),*/
        array(
            'name'          => 'Toggle menu',
            'slug'          => 'wp-clean-admin-menu',
            'required'      => 'false',
        ),     
        array(
            'name'          => 'W3 Total cache',
            'slug'          => 'w3-total-cache',
            'required'      => false,
        ),
        array(
            'name'          => 'CPTUI',
            'slug'          => 'custom-post-type-ui',
            'required'      => false,
        ),
        array(
            'name'          => 'Simple history',
            'slug'          => 'simple-history',
            'required'      => false,
        ),
        array(
            'name'          => 'Duplicate post',
            'slug'          => 'duplicate-post',
            'required'      => false,
        ),
        /*array(
            'name' => 'WP Rocket', // Le nom du plugin
            'slug' => 'wp-rocket_2.8.23', // Le slug du plugin (généralement le nom du dossier)
            'source' => dirname( __FILE__ ) . '/plugins/wp-rocket_2.8.23.zip', // le chemin relatif du plugin au format .zip
            'required' => false, // FALSE signifie que le plugin est seulement recommandé
        ),*/
        array(
            'name' => 'Elementor PRO', // Le nom du plugin
            'slug' => 'elementor-pro', // Le slug du plugin (généralement le nom du dossier)
            'source' => dirname( __FILE__ ) . '/plugins/elementor-pro-3.20.0.zip', // le chemin relatif du plugin au format .zip
            'required' => false, // FALSE signifie que le plugin est seulement recommandé
        ),
        array(
            'name' => 'ACF PRO', // Le nom du plugin
            'slug' => 'advanced-custom-fields-pro.zip', // Le slug du plugin (généralement le nom du dossier)
            'source' => dirname( __FILE__ ) . '/plugins/advanced-custom-fields-pro.zip', // le chemin relatif du plugin au format .zip
            'required' => false, // FALSE signifie que le plugin est seulement recommandé
            'has_notices' => false,
        ),
        array(
            'name' => 'WPML', // Le nom du plugin
            'slug' => 'otgs-installer-plugin.3.1.3.zip', // Le slug du plugin (généralement le nom du dossier)
            'source' => dirname( __FILE__ ) . '/plugins/otgs-installer-plugin.3.1.3.zip', // le chemin relatif du plugin au format .zip
            'required' => false, // FALSE signifie que le plugin est seulement recommandé
            'has_notices' => false,
        ),
    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id'           => 'wp-visible',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'plugins.php',            // Parent menu slug.
        'capability'   => 'manage_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
        'message'      => 'Visible recommande ces plugins : ',                      // Message to output right before the plugins table.

        /*
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'wp-visible' ),
            'menu_title'                      => __( 'Install Plugins', 'wp-visible' ),
            /* translators: %s: plugin name. * /
            'installing'                      => __( 'Installing Plugin: %s', 'wp-visible' ),
            /* translators: %s: plugin name. * /
            'updating'                        => __( 'Updating Plugin: %s', 'wp-visible' ),
            'oops'                            => __( 'Something went wrong with the plugin API.', 'wp-visible' ),
            'notice_can_install_required'     => _n_noop(
                /* translators: 1: plugin name(s). * /
                'This theme requires the following plugin: %1$s.',
                'This theme requires the following plugins: %1$s.',
                'wp-visible'
            ),
            'notice_can_install_recommended'  => _n_noop(
                /* translators: 1: plugin name(s). * /
                'This theme recommends the following plugin: %1$s.',
                'This theme recommends the following plugins: %1$s.',
                'wp-visible'
            ),
            'notice_ask_to_update'            => _n_noop(
                /* translators: 1: plugin name(s). * /
                'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
                'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
                'wp-visible'
            ),
            'notice_ask_to_update_maybe'      => _n_noop(
                /* translators: 1: plugin name(s). * /
                'There is an update available for: %1$s.',
                'There are updates available for the following plugins: %1$s.',
                'wp-visible'
            ),
            'notice_can_activate_required'    => _n_noop(
                /* translators: 1: plugin name(s). * /
                'The following required plugin is currently inactive: %1$s.',
                'The following required plugins are currently inactive: %1$s.',
                'wp-visible'
            ),
            'notice_can_activate_recommended' => _n_noop(
                /* translators: 1: plugin name(s). * /
                'The following recommended plugin is currently inactive: %1$s.',
                'The following recommended plugins are currently inactive: %1$s.',
                'wp-visible'
            ),
            'install_link'                    => _n_noop(
                'Begin installing plugin',
                'Begin installing plugins',
                'wp-visible'
            ),
            'update_link'                     => _n_noop(
                'Begin updating plugin',
                'Begin updating plugins',
                'wp-visible'
            ),
            'activate_link'                   => _n_noop(
                'Begin activating plugin',
                'Begin activating plugins',
                'wp-visible'
            ),
            'return'                          => __( 'Return to Required Plugins Installer', 'wp-visible' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'wp-visible' ),
            'activated_successfully'          => __( 'The following plugin was activated successfully:', 'wp-visible' ),
            /* translators: 1: plugin name. * /
            'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'wp-visible' ),
            /* translators: 1: plugin name. * /
            'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'wp-visible' ),
            /* translators: 1: dashboard link. * /
            'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'wp-visible' ),
            'dismiss'                         => __( 'Dismiss this notice', 'wp-visible' ),
            'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'wp-visible' ),
            'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'wp-visible' ),

            'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
        ),
        */
    );

    tgmpa( $plugins, $config );
}

//translations for tgmpa
function load_langs_tgmpa() {
    load_theme_textdomain( 'tgmpa', dirname( __FILE__ )  . '/languages' );
}
add_action( 'init', 'load_langs_tgmpa' , 1 );

?>